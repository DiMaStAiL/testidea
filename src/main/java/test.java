import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class test {
    public WebDriver driver;
    WebElement object;

    @Test(priority=1)
    void pageLoaded(){
        driver.get("https://demo.opencart.com/index.php?route=common/home");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        WebElement title = driver.findElement(By.xpath("//h1/a[text()='Your Store']"));
        boolean actual = title.isDisplayed();
        Assert.assertTrue(actual, "Title is not displayed");
    }

    @Test(priority=2)
    void searchTest(){
        object = driver.findElement(By.xpath("//input[@name='search']"));
        object.click();
        object.sendKeys("mac");
        object.sendKeys(Keys.ENTER);

        List<WebElement> product = driver.findElements(By.xpath("//div[@class='product-thumb']"));

        boolean actual = false;
        if (product.size() == 4) {
            actual = true;
        }
        Assert.assertTrue(actual, "Product is not find");
    }

    @Test(priority=3)
    void sortCheck(){
        object = driver.findElement(By.xpath("//select[@name='category_id']"));
        boolean check = object.isDisplayed();
        Assert.assertTrue(check, "Sort is not find");
    }

    @Test(priority=4)
    void sortTest(){
        object = driver.findElement(By.xpath("//select[@name='category_id']"));
        object.click();
        object = driver.findElement(By.xpath("//option[text()='Price (High > Low)']"));
        object.click();

        List<WebElement> productPrise = driver.findElements(By.xpath("//p[@class='price']"));
        ArrayList<Double> listPrice = new ArrayList<Double>();

        for (WebElement i: productPrise) {
                String b = i.getText();
                String[] price = b.split("\n");
                b = price[0];
                b = b.replaceAll("[,$\"]", "");
                Double a = Double.parseDouble(b);
                listPrice.add(a);
//                System.out.println(a);
        }
        boolean check = false;
        Double num = listPrice.get(0);
        for (Double i: listPrice) {
            if(num >= i){
                num = i;
                check = true;
            }
            else{
                check = false;
                break;
            }
        }
        Assert.assertTrue(check, "Sort is not true");
    }





//    @Test(priority=3)
//    void cartOneItem() throws InterruptedException {
//        WebElement mac = driver.findElement(By.xpath("//img[@title='MacBook']"));
//        mac.click();
//        WebElement add = driver.findElement(By.xpath("//button[@id='button-cart']"));
//        add.click();
//        boolean actual = false;
//        for(int i=0;i<10;i++){
//            cart = driver.findElement(By.xpath("//span[@id='cart-total']"));
//            actual = cart.getText().contains("1 item(s)");
//            if(actual){break;}else {
//                Thread.sleep(50);
//            }
//        }
//
//        Assert.assertTrue(actual, "Cart does not contain 1 item");
//
//    }

    @BeforeClass
    void before(){
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @AfterClass
    void postConditions(){
        driver.quit();
    }
}
